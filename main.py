from bs4 import BeautifulSoup as bs
import requests
import json

r = requests.get("https://free-proxy-list.net/")

table = bs(r.content, "html5lib").select_one("#proxylisttable")

# extracting cell of each row of table
rows = [ row for row in table("tr")]

header = [cell.text for cell in rows[0]]
body = [[cell.text for cell in row('td')] for row in rows]

data = []
for line in body:
	aux = {}
	if len(line) != len(header):
		continue
	for i, col in enumerate(header):
		aux[col] = line[i]
	data.append(aux)

with open("data.json", "w") as f:
	f.write(json.dumps(data))
