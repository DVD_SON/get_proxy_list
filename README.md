# Download Proxy-List

Este projeto tem como objetivo baixar toda a tabela de proxies apresentando na página https://free-proxy-list.net/ de forma automatizada e armazenar em um arquivo json. 

## Descrição do teste

Este teste consiste no desenvolvimento de um script simples, para coletar dados da web.
A escolha da ferramenta para realizar o processo é livre, desde que consiga realizar a
coleta de dados de forma eficiente. Algumas bibliotecas interessantes para realizar essa
tarefa são: BeautifulSoup, lxml, Scrapy, Selenium, etc.

Proposta:
O Script deve acessar a url: https://free-proxy-list.net/ e coletar todas as informações
tabeladas, que são:
● IP Address
● Port Code
● Country
● Anonymity
● Google
● Https
● Last Checked
São 15 páginas de informação. Seu script deve acessar todas as páginas de forma
totalmente automatizada e salvar todos os dados no formato json, em um arquivo chamado
‘data.json’.
Solicito também que me envie um link do git, com um projeto seu usando python. De
preferência usando Django.
Envie a resolução para: marcos@fontedeprecos.com.br
Até Quinta Feira 20/02/2020, 8h da manhã.
Dúvidas.: 82 991946985 (whatsapp/telegram) ou o email acima.



Essa descrição também pode ser lida no pdf teste.pdf que se encontra na pasta principal do projeto.



## Como executar

#### Dependências

Para controle de dependências este projeto usou pipenv, tenha certeza de o ter instalado no sistemas, assim para instalar as dependências é preciso executar o comando:

```bash
pipenv install
```

#### Executando o Código

Após instalar as dependências o script poderá ser executado, use a linha de comando:

```bash
pipenv run python main.py
```

O resultado será um arquivo data.json contendo a lista de linha pertencentes a tabela solicitada.